package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner studentScanner = new Scanner(System.in);
        System.out.println("Enter your First Name:");
        firstName = studentScanner.nextLine().trim();
        System.out.println("Enter your Last Name:");
        lastName = studentScanner.nextLine().trim();
        System.out.println("Enter your First Subject grade:");
        firstSubject = studentScanner.nextDouble();
        System.out.println("Enter your Second Subject grade:");
        secondSubject = studentScanner.nextDouble();
        System.out.println("Enter your Third Subject grade:");
        thirdSubject = studentScanner.nextDouble();
        System.out.println("Hi! " + firstName + " " + lastName);
        System.out.println("Your grades are:");
        ArrayList<Double> grades = new ArrayList<>();
        grades.add(firstSubject);
        grades.add(secondSubject);
        grades.add(thirdSubject);
        System.out.println(grades);
        double avg = (firstSubject + secondSubject + thirdSubject)/3;
        System.out.println("Your average is: " + avg);
    }
}
