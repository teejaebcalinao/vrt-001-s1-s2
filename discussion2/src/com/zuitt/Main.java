package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int num1 = 10;
        int num2 = 20;

        if(num1>5)
            System.out.println("Num1 is greater than 5");

        boolean isHandsome = true;

        if(isHandsome)
            System.out.println("Hello, Tee Jae");

        boolean isMarried = false;
        boolean isTwice = true;

        if(isMarried == false && isTwice == true)
            System.out.println("Kim Dahyun is life.");

        String  word = "hello";

        if(word.equalsIgnoreCase("Hello"))
            System.out.println("Is it me you're looking for?");

        String word2 = " ";
        if(word2.isBlank())
            System.out.println("Answer ME!");

        System.out.println(word2.isEmpty());

        System.out.println(word.length());

        int x = 15;
        int y = 0;

        //System.out.println(x/y == 0);

        if(y>5&& x/y == 0)
            System.out.println("result is: " + x/y);
        else
            System.out.println("Short circuited.");

        Scanner appScanner = new Scanner(System.in);
/*
        System.out.println("Provide a number between 1-2");

        int directionVal = appScanner.nextInt();

        switch (directionVal){
            case 1:
                System.out.println("SM North Edsa");
                break;
            case 2:
                System.out.println("SM South Mall");
                break;
            default:
                System.out.println("Out of range");
                break;
        }

        switch (directionVal){
            case 1,3 -> System.out.println("Malapit sa Trinoma");
            case 2,4 -> System.out.println("Malapit sa ATC");
            default -> System.out.println("Out of range, beau.");
        }

        String response = switch(directionVal){
            case 1:
                yield "North";
            case 2:
                yield "South";
            default:
                yield "Invalid";
        };

        System.out.println(response);*/

        for (int i = 0; i <= 5; i++){
            System.out.println(i);
        }

        String[] nameArr = {"Dahyun","Tzuyu","Chaeyoung"};

        for(String name : nameArr){
            System.out.println(name);
        }

/*        String name = "";

        while(name.isBlank()){
            System.out.println("Beh, anpangalan m?");
            name = appScanner.nextLine();


        }*/

        //exception handling
/*        System.out.println("Input a number");
        int num = appScanner.nextInt();
        System.out.println(num);

        System.out.println("Input a decimal number:");
        double decimal = appScanner.nextDouble();
        System.out.println(decimal);*/

        int number = 0;

/*        try{
            System.out.println("Input a number:");
            number = appScanner.nextInt();

        } catch (InputMismatchException e){
            System.out.println("Input type incorrect");
        } catch (Exception e){
            System.out.println("Invalid input");
            System.out.println(e);
            e.printStackTrace();
        }
        finally {
            System.out.println("The number you entered is " + number);
        }*/

/*        try {
            multipleExceptions(appScanner);
        } catch (ArithmeticException e){
            System.out.println("Don't divide by Zero");
        } catch (NullPointerException e){
            System.out.println("String can't be null");
        } finally {
            System.out.println("Thanks for playing.");
        }*/

        try {
            System.out.println("Give Number");
            int userInput = appScanner.nextInt();
            if(userInput == 9){
                throw new NewException("Your syntax is correct", appScanner);
            }
        } catch (NewException e){
            System.out.println(e);
        } finally {
            System.out.println("Wooot");
        }

        //user defined exception
    }

    public static void multipleExceptions(Scanner appScanner) throws ArithmeticException, NullPointerException{

        System.out.println("Which error would you like to see? [1] Arithmetic Exception, [2] NullPointerException");
        int option = appScanner.nextInt();
        if (option ==1){
            int divideByZero = 10/0;
        } else if (option == 2){
            String text = null;
            System.out.println(text.length());
        }

    }

}

//user defined exception

class NewException extends Exception {
    public NewException(String message, Scanner appScanner){

        super(message);
        System.out.println("Give another number");
        int myNum = appScanner.nextInt();
        System.out.println(myNum);

    }
}
