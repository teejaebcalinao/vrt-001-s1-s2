package com.zuitt;

public class User {

    private String name;
    private String joinDate;
    private Boolean isAdmin;

    public User() {
    }

    public User(String name, String joinDate, Boolean isAdmin) {
        this.name = name;
        this.joinDate = joinDate;
        this.isAdmin = isAdmin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public void login(){
        System.out.println("User has logged in.");
    }
    public void checkout(){
        System.out.println("Thank you! Proceed To Payment.");
    }

}
