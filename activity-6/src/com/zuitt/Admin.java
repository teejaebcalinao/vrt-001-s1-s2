package com.zuitt;

public class Admin extends User{

    private String role;

    public Admin() {
    }

    public Admin(String name, String joinDate, Boolean isAdmin, String role) {
        super(name, joinDate, isAdmin);
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void addProduct(String productName, String description, double price){
        System.out.println("Admin " + getName() + " has created:");
        System.out.println("Product Name:");
        System.out.println(productName);
        System.out.println("Description:");
        System.out.println(description);
        System.out.println("Price:");
        System.out.println(price);
    }
}
