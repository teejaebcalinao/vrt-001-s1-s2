package com.zuitt;

public class Nokia {


    private String model;
    private int price;

    public Nokia() {
    }

    public Nokia(String model, int price) {
        this.model = model;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void sendText(String message,String recipient){

        System.out.println("To " + recipient + ": ");
        System.out.println(message);

    }

    public void playMusic(String songName){

        System.out.println("You are listening to: ");
        System.out.println(songName);

    }

}
