package com.zuitt;

public class Regular extends User{

    public Regular() {
    }

    public Regular(String name, String joinDate, Boolean isAdmin) {
        super(name, joinDate, isAdmin);
    }

    public void login(){
        System.out.println("Thank you for logging in, User.");
        System.out.println("You may join our Premium Membership for 20% discount.");
    }

    public void checkout(){
        System.out.println("Thank you for checking out.");
        System.out.println("You may enjoy 20% discount if you join our Premium membership.");
    }
}
