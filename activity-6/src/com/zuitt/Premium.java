package com.zuitt;

public class Premium extends User {

    public Premium() {
    }

    public Premium(String name, String joinDate, Boolean isAdmin) {
        super(name, joinDate, isAdmin);
    }

    public void login() {
        System.out.println("Thank you for logging in, Premium User!");
    }

    public void checkout(){
        System.out.println("As a premium member, you will get a 20% discount.");
    }
}
