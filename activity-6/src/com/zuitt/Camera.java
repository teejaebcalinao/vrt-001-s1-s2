package com.zuitt;

public interface Camera {

    void takePicture(String caption);
    void takeVideo(String description);

}
