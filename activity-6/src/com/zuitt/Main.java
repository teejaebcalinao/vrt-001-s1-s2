package com.zuitt;

public class Main {

    public static void main(String[] args) {

        Nokia newCell1 = new Nokia("N95",5000);
        newCell1.sendText("Here na me, wer na u?","Miles");
        newCell1.playMusic("Thunder - Boys Like Girls");

        Samsung newCell2 = new Samsung("A91",30000);
        newCell2.sendText("Kumain ka na?","Tina");
        newCell2.playMusic("Miss Independent - Ne-Yo");
        newCell2.takePicture();
        newCell2.takeVideo();

        Admin newAdmin = new Admin("Tina","12-26-1990",true,"Admin");
        newAdmin.addProduct("Cetaphil","Moisturizing Lotion",76);

        Regular newUser = new Regular("Jimmy","08-06-1991",false);
        newUser.login();
        newUser.checkout();

        Premium newUser2 = new Premium("Markus","03-22-2022",false);
        newUser2.login();
        newUser2.checkout();


    }
}
