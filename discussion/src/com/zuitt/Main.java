package com.zuitt;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");

        //Java Variable Int
        int myNum = 25;
        System.out.println(myNum);

        //Java Variable String
        String myName;
        myName = "Initialize";
        System.out.println(myName);

        if(myNum > 20){
            System.out.println(myName);
        }

        //constant
        final int PRINCIPAL = 3000;
        final Double PI = 3.1416;

        //data types
        char letter = 'a';
        boolean isMVP = true;

        byte students = 127;
        short seats = 32767;
        int localPopulation = 2_147_483_647;
        long worldPopulation = 7_862_881_145L;

        //float and double
        float price = 75.20F;
        double temperature = 37.5423123;

        //get data type - primitive
        System.out.println(((Object)temperature).getClass());

        //non-primitive/referencing data types
        String name = "John Doe";
        System.out.println(name);

        String editedName = name.toLowerCase();
        System.out.println(editedName);

        //escape sequence
        System.out.println("c:\\windows\\desktop");
        System.out.println(name.getClass());

        //arithmetic operations
        int result = 10 + 15;
        System.out.println(result);

        //casting/type casting
            //converting data type to another

        //implicit casting
        //data type will convert implicitly
            //byte > short > int > long > float > double

        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println(total);

        //explicit casting
        int newTotal = num1 + (int)num2;
        System.out.println(newTotal);

        //string to int
        String sampleStr = "50";
        String sampleStr2 = "65";

        System.out.println(Integer.parseInt(sampleStr) + Integer.parseInt(sampleStr2));

        int totalSample = Integer.parseInt(sampleStr) + Integer.parseInt(sampleStr2);
        System.out.println(totalSample);

        String stringSample2 = Integer.toString(totalSample);
        System.out.println(stringSample2.getClass());

        //scanner  - prompt
//        Scanner appScanner = new Scanner(System.in);
//
//        System.out.println("What's your name?");
//
//        String newName = appScanner.nextLine().trim();
//        System.out.println(newName);
//
//        int myAge = appScanner.nextInt();

        //arrays
            //group of data

        int[] intArray = new int[3];
        intArray[0] = 10;
        intArray[1] = 15;
        intArray[2] = 20;

        System.out.println(intArray);
        System.out.println(intArray[1]);

        //java array cannot be pushed into.
        //length is fixed from initialization.

        int[] intArray2 = {20,15,25,30};
        System.out.println(intArray2[1]);

        System.out.println(Arrays.toString(intArray));
        //sorts alphanumeric
        Arrays.sort(intArray2);
        System.out.println(Arrays.toString(intArray2));

        String searchTerm = "Joe";
        String[] names = {"John","Kim","Joe","Coolio"};
        int resultStr = Arrays.binarySearch(names,searchTerm);
        System.out.println(resultStr);
        //mutator / non-mutator

        //Multidimensional Array
        String[][] classroom = new String[3][3];

                // First Row
                classroom[0][0] = "Athos";
                classroom[0][1] = "Porthos";
                classroom[0][2] = "Porthos";

                // Second Row
                classroom[1][0] = "Mickey";
                classroom[1][1] = "Donald";
                classroom[1][2] = "Goofy";

                // Third Row
                classroom[2][0] = "Harry";
                classroom[2][1] = "Ron";
                classroom[2][2] = "Hermione";

        System.out.println(classroom);
        System.out.println(Arrays.deepToString(classroom));

        //arrayLists
            //resizable array

        ArrayList<String> classList = new ArrayList<>();

        //arrayList methods
        classList.add("Paul");
        classList.add("Jeff");

        //retrieve elements
        System.out.println(classList.get(0));
        System.out.println(classList.get(1));

        //change elements
        //arrayList.set(indexNum,"new value")
        classList.set(1,"Richard");
        System.out.println(classList.get(1));

        //removing elements
        //arrayList.remove(indexNum)
        classList.remove(1);

        //clear arraylist
        classList.clear();
        System.out.println(classList);

        //get length
        System.out.println(classList.size());

        //arrayLists with initial value

        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Mary","Jane"));
        System.out.println(employees);

        //hashmaps

        HashMap<String,String> employeeRole = new HashMap<>();

        //CRUD
            //Add fields
            employeeRole.put("Brandon","Student");
            employeeRole.put("Alice","Bulakbol");
            System.out.println(employeeRole);

            //retrieve value
            System.out.println(employeeRole.get("Alice"));

            //delete
            employeeRole.remove("Brandon");
            System.out.println(employeeRole);

            //retrieve key
            System.out.println(employeeRole.keySet());


        //hashmaps with arrayList

        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradeListA = new ArrayList<>(Arrays.asList(5,10,45));
        ArrayList<Integer> gradeListB = new ArrayList<>(Arrays.asList(15,20,35));

        subjectGrades.put("Joe",gradeListA);
        subjectGrades.put("Biden",gradeListB);

        System.out.println(subjectGrades);

        //access joe values
        System.out.println(subjectGrades.get("Joe").get(2));
        System.out.println(subjectGrades.get("Biden").get(0));


    }
}
