package com.zuitt;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        User newUser = new User("Mary","Watson","mary@gmail.com","09266772400");
        System.out.println(newUser.getFirstName());
        newUser.printDetails();
        Scanner appScanner = new Scanner(System.in);
        System.out.println("Enter First Name:");
        String firstName = appScanner.nextLine().trim();
        System.out.println("Enter Last Name:");
        String lastName = appScanner.nextLine().trim();
        System.out.println("Enter Email:");
        String email = appScanner.nextLine().trim();
        System.out.println("Enter Contact:");
        String contact = appScanner.nextLine().trim();
        User newUser2 = new User(firstName,lastName,email,contact);
        newUser2.printDetails();
    }
}

