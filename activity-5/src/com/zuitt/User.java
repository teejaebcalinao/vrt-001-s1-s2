package com.zuitt;

public class User {

    public String firstName;
    public String lastName;
    public String email;
    public String contactNumber;
    private String id;

    public User(){}

    public User(String firstName, String lastName, String email, String contactNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email  = email;
        this.contactNumber = contactNumber;
        int numId = (int) (Math.random()*100);
        int numId2 = (int) (Math.random()*100);
        this.id = "user-"+ numId + numId2;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getId() {
        return id;
    }

    //methods
    public void introduce(){
        System.out.println("Hi! My name is: " + firstName + " " + lastName);
    }

    public void printDetails(){
        System.out.println("Id: " + id);
        System.out.println("First Name: " + firstName);
        System.out.println("Last Name: " + lastName);
        System.out.println("Email: " + email);
        System.out.println("Contact: " + contactNumber);
    }

}
