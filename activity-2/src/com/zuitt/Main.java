package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String[] fruits = {"apple","banana","avocado","kiwi","orange"};

        Scanner myScanner = new Scanner(System.in);
        System.out.println("What are you looking for?");
        String fruit = myScanner.nextLine().trim().toLowerCase();
        Arrays.sort(fruits);
        System.out.println(Arrays.toString(fruits));
        int resultStr = Arrays.binarySearch(fruits,fruit);
        System.out.println(fruit + " is at index number: " + resultStr);

        //hashmaps
        HashMap<String,String> user1 = new HashMap<>();
        user1.put("firstName","Errol");
        user1.put("lastName","Flynn");
        HashMap<String,String> user2 = new HashMap<>();
        user2.put("firstName","Robin");
        user2.put("lastName","Hood");

        ArrayList<HashMap<String,String>> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        System.out.println(users);

        //stretch
        System.out.println("Input a number to look for user details");
        int userOrder = myScanner.nextInt();
        System.out.println(users.get(userOrder-1));
    }
}
