package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("Input year");
        int year = appScanner.nextInt();

        if(year % 4 == 0){

            if(year % 100 == 0){

                if(year % 400 == 0){

                    System.out.println(year + " is a leap year");

                } else {

                    System.out.println(year + " is NOT a leap year");

                }

            } else {

                System.out.println(year + " is a leap year");
            }

        } else {

            System.out.println(year + " is NOT a leap year");

        }

        ArrayList<String> users = new ArrayList<>(Arrays.asList("Superman","Batman","Plastic Man","Green Lantern","The Flash"));
        System.out.println(users);
        ArrayList<String> filteredUsers = new ArrayList<>();
        Scanner newScanner = new Scanner(System.in);
        System.out.println("Find users");
        String searchItem = " ";
        while(searchItem.isBlank()){
            searchItem = newScanner.nextLine().toLowerCase().trim();
            for(String user : users){
                if(user.contains(searchItem)){
                   filteredUsers.add(user);
                }
            }
        }
        System.out.println("Users Found:");
        System.out.println(filteredUsers);




    }
}
