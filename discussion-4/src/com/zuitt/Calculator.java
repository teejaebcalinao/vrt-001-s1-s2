package com.zuitt;

public interface Calculator {

    void compute(String numA, String numB, String operation);
    void turnOff();

}
