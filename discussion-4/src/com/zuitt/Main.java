package com.zuitt;

public class Main {

    public static void main(String[] args) {

    /*
    [Section] Encapsulation
    - Encapsulation is the mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
    - Unlike in procedural programming languages like JavaScript where code is written when necessary following a specific flow of the script, Java separates its code into classes
    */

 /*       String petAName = "Franky";
        String petAGender = "Female";
        String petAClassification = "Dog";
        int petAAge = 10;
        String petAAddress = "Manila, Philippines";
        String petASound = "Bark!";

        describePet(petAName, petAGender, petAClassification, petAAge);
        makeSound(petAName, petASound);

        String petBName = "Simba";
        String petBGender = "Male";
        String petBClassification = "Lion";
        int petBAge = 1;
        String petBAddress = "Pride Lands";
        String petBSound = "Rawr!";

        describePet(petBName, petBGender, petBClassification, petBAge);
        makeSound(petBName, petBSound);
*/

        Pet petA = new Pet("Franky", "Female", "Dog", 10, "Manila, Philippines", "Bark!");

        petA.describePet();
        petA.makeSound();

        Pet petB = new Pet("Simba", "Male", "Lion", 1, "Pride Lands", "Rawr!");
        petB.describePet();
        petB.makeSound();

        Casio myCasio = new Casio("Casio",200);
        myCasio.compute("33","45","divide");

        Sharp mySharp = new Sharp("Sharp",1000);
        mySharp.compute("23","20","divide");

        useCalculator(myCasio);
        useCalculator(mySharp);

        //inheritance

        Crocodile myPet = new Crocodile();
        myPet.setClassification("Reptile");
        myPet.setDietType("Carnivore");
        myPet.setHabitat("Fresh Water");
        myPet.setHasScales(true);
        myPet.setName("Drogon");
        myPet.setAge(7);

        myPet.describePet();
        myPet.swim();
        myPet.sleep();
        myPet.eat();

        Driver myDriver = new Driver("The Transporter",45);
        Car myCar = new Car("Rolls Royce","Phantom",myDriver);

        System.out.println(myCar.getMake());
        System.out.println(myCar.getDriver().getName());

        Turtle myOtherPet = new Turtle("Reptile","Herbivore","Salt Water",false,"Regal",5);

        myOtherPet.swim();
        myPet.swim();

    }

    public static void useCalculator(Calculator calculator){
        calculator.compute("20","491","add");
        calculator.turnOff();
    }
}
