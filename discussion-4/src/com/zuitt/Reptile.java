package com.zuitt;

public class Reptile extends Animal {

    private String habitat;
    private boolean hasScales;

    public Reptile(){}

    public Reptile(String classification, String dietType, String habitat, boolean hasScales) {
        super(classification,dietType);
        this.habitat = habitat;
        this.hasScales = hasScales;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public boolean getHasScales() {
        return hasScales;
    }

    public void setHasScales(boolean hasScales) {
        this.hasScales = hasScales;
    }

    public void eat(){
        System.out.println("This animal is eating.");
    }
}
