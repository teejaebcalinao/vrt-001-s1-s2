package com.zuitt;

public class Crocodile extends Reptile{

    private String name;
    private int age;

    public Crocodile() {
    }

    public Crocodile(String classification, String dietType, String habitat, boolean hasScales, String name, int age){

        super(classification,dietType,habitat,hasScales);
        this.name = name;
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void describePet() {
        System.out.println("Name: " + getName());
        System.out.println("Age: " + getAge());
        System.out.println("Classification: " + getClassification());
        System.out.println("Diet Type: " + getDietType());
        System.out.println("Habitat: " + getHabitat());
        System.out.println("Has Scales: " + getHasScales());
    }

    public void swim() {
        System.out.println("swim splash gurgle gurgle");
    }
    //override
    public void sleep(){
        System.out.println(getName() + " is ZzzZzzzz");
    }
    //overload
    public void eat(String food){
        System.out.println(getName() + " is eating " + food + ".");
    }

}
