package com.zuitt;

public class Car {

    private String make;
    private String model;
    private Driver driver;

    public Car() {
    }

    public Car(String make, String model, Driver driver) {
        this.make = make;
        this.model = model;
        this.driver = driver;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void start(){
        System.out.println("Rriiindiigdigdig riiiindiigdigdig vroom vroom");
    }
}
