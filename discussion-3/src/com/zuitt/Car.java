package com.zuitt;

public class Car {
    //components of class:

    //properties

    private String name;
    private String brand;
    private int manufactureDate;
    private String owner;

    //constructors
        //empty
        public Car(){}

        //parameterized constructor
        public Car(String name,String brand,int manufactureDate,String owner){
            this.name = name;
            this.brand = brand;
            this.manufactureDate = manufactureDate;
            this.owner = owner;
        }

    //getters and setters
        public String getName(){
            return this.name;
        }

        public void setName(String name){
            this.name = name;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public int getManufactureDate() {
            return manufactureDate;
        }

        public void setManufactureDate(int manufactureDate) {
            this.manufactureDate = manufactureDate;
        }

        public String getOwner() {
            return owner;
        }

    //Methods

    public void drive(){
        System.out.println("Vroom Vroom.");
    }

    public void printDetails(){
        System.out.println("Name: " + getName() + ", Brand: " + getBrand() + ", Date: " + getManufactureDate() + ", Owner: " + getOwner() );
    }
}
