package com.zuitt.accessmodifiers;

public class Example {
    public String message = "Hello from public";

    String defaultMessage = "This cannot be accessed outside of the default class.";

    protected String protectedMessage = "This is a protected message";

    private String privateMessage = "This is a private message";

}
