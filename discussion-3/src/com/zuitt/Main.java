package com.zuitt;

import com.zuitt.accessmodifiers.DiffExample;
import com.zuitt.accessmodifiers.Example;

public class Main {

    public static void main(String[] args) {

        Car myCar = new Car();
        System.out.println(myCar);

//        myCar.name = "Civic";
//        myCar.brand = "Honda";
//        myCar.manufactureDate = 1998;
//
//        System.out.println(myCar.name);
//        System.out.println(myCar.brand);
//        System.out.println(myCar.manufactureDate);
//        System.out.println(myCar);

        myCar.setName("Bagong Car");
        System.out.println(myCar.getName());
        myCar.setBrand("Bagong Brand");
        System.out.println(myCar.getBrand());
        myCar.setManufactureDate(1892);
        System.out.println(myCar.getManufactureDate());


        Car newCar = new Car("Charger","Dodge",1970,"Jimmy");
        System.out.println(newCar.getName());
        System.out.println(newCar.getOwner());
        //newCar.owner = "Paul";
        //newCar.setOwner = "P. Walker";

        newCar.drive();
        newCar.printDetails();

        myCar.drive();
        myCar.printDetails();

        Example accessModifier = new Example();
        System.out.println(accessModifier.message);
       // System.out.println(accessModifier.defaultMessage);
       // System.out.println(accessModifier.protectedMessage);
        //System.out.println(accessModifier.privateMessage);
        DiffExample defaultModifier = new DiffExample();
        defaultModifier.printMessage();

        Protected protectedAccess = new Protected();
        protectedAccess.printProtectedMsg();

    }

    public static class Protected extends Example {
        public void printProtectedMsg(){
            System.out.println(protectedMessage);
            //System.out.println(privateMessage);
        }
    }

}
